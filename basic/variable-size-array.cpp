// hackerrank

#include <vector> 
#include <iostream> 
using namespace std;

int main() {
    int n, q, value, size, which, index;
    cin >> n >> q;
    vector<vector<int> > vec;
    for (int i = 0; i < n; i++) {
        cin >> size; 
        vector<int> ivec;
        for (int j = 0; j < size; j++) {
            cin >> value; 
            ivec.push_back(value);
        }
        vec.push_back(ivec);
    }

    for(int j = 0; j < q; j++) {
        cin >> which >> index;
        cout << vec[which][index] << endl;
    }
    return 0;
}