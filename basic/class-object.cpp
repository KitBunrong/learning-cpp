#include <iostream> 
using namespace std;

class Student{
    private:
        int s[5], sum;
    public: 
        Student() : sum(0){};
        int calculateTotalScore() {return sum;};
        void input() {
            for(int i = 0; i < 5; i++) {
                cin >> s[i];
                sum += s[i];
            }
        }
};

int main() {
    // Number of student
    int n;
    cin >> n;

    // Arrays of N students
    Student *s = new Student[n];

    for (int i = 0; i < n; i++) {
        s[i].input();
    }

    // Calculate Kristen's score
    int kristen_score = s[0].calculateTotalScore();

    // Determine how many student higher than Kristen
    int count = 0;
    for (int i = 1; i < n; i++) {
        int total = s[i].calculateTotalScore();
        if (total > kristen_score ) {
            count++;
        }
    }

    // Print result
    cout << count;

    return 0;
}