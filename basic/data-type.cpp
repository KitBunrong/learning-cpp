// hackerrank

#include <iostream> 
#include <cstdio>
using namespace std;

int main() {
    int i;
    long l;
    char c;
    float f;
    double d;
    scanf("%d %ld %c %f %ld", &i, &l, &c, &f, &d);
    printf("%d \n%ld \n%c \n%f \n%ld", i, l, c, f, d);
    return 0;
}