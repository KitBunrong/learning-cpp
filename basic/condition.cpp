// hackerrank

#include <bits/stdc++.h>
using namespace std;

int main() {
    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    if(n == 1) {
        printf("one");
    } else if(n == 2) {
        printf("two");
    } else if (n == 3) {
        printf("three");
    } else {
        printf("match no found");
    }
    return 0;
}